FROM --platform=$BUILDPLATFORM golang:alpine AS builder

WORKDIR /go/src/app

COPY . .

RUN apk add make dep git
RUN make build

FROM alpine:latest

EXPOSE 1234

ENV \
    VAULT_ADDR \
    VAULT_TOKEN

RUN \
apk add --no-cache ca-certificates ;\
mkdir -p /opt/supersecret/static

WORKDIR /opt/supersecret

COPY --from=builder /go/src/app/bin/sup3rs3cretMes5age /opt/supersecret
COPY static /opt/supersecret/static

CMD [ "./sup3rs3cretMes5age" ]
